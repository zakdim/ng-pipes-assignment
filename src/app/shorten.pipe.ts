import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
  name: 'shorten'
})
export class ShortenPipe implements PipeTransform {

  transform(value: any, limit: number) {
    let _limit = limit || 10;
    if (value.length > _limit) {
      return value.substr(0, _limit) + ' ...';
    } else
    return value;
  }
}
